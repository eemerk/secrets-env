= Secrets in environment variables

//tag::abstract[]

Secrets stored in an environment variable are prone to 
accidental exposure.

//end::abstract[]

Environment variables are:
. Implicitly exposed to the processes
. Grabbed and printed out by processes as debug or error messages
. Passed down to child processes

//tag::lab[]

== Lab

Your objective in each lab is to pass the build.

=== Task 0

Fork and clone this repository.
Install `docker`, `docker-compose` and `make` on your system.

. Run unit tests: `make test`. 
. Build the program: `make build`.
. Run it: `make run`.
. Run security tests: `make securitytest`.

Note: The last test will fail. 


=== Task 1

Review `Dockerfile` and `docker-compose.yml` file. 
`APPKEY` secret is exposed as an environment variable.
Find out the security issue and why tests fails.

Note: Avoid looking at tests and try to
spot the vulnerability on your own.

=== Task 2

Remove `APPKEY` from environment variable.
Can you find a feature in Docker to securely handle and supply
`APPKEY` to the container?

== Task 3

Modify both `docker-compose.yml` and `Dockerfile`
to make use of Docker Secret.
Run security tests again. Make sure build will pass.
If you stuck, move to the next task.

=== Task 4 
 
Check out the `patch` branch and review the changes. 
Run all tests and make sure everything pass. 

=== Task 5 
 
Merge the patch branch to master. 
Commit and push your changes. 
Does pipeline for your forked repository show that you have passed the build?   

(Note: you do NOT need to send a pull request)

//end:lab[]

//tag::references[]

== References

* https://diogomonica.com/2017/03/27/why-you-shouldnt-use-env-variables-for-secret-data/

//end::references[]
